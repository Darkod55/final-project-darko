<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->renameColumn('name', 'name_en');
            $table->string('name_mk')->after('name');
            $table->renameColumn('search_word', 'search_word_en');
            $table->string('search_word_mk')->after('search_word');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn(['name_mk']);
            $table->dropColumn(['search_word_mk']);
            $table->renameColumn('name_en', 'name');
            $table->renameColumn('search_word_en', 'search_word');
        });
    }
}
