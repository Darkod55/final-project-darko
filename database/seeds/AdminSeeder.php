<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')
        ->insert([
            ['name' => 'Darko', 'email' => "darkod55@yahoo.com", "role_id" => "1", 'password' => bcrypt('Darkod55'), 'api_token' => \Str::random(80)],
            ['name' => 'Admin', 'email' => "test@yahoo.com", "role_id" => "1", 'password' => bcrypt('Testing123'), 'api_token' => \Str::random(80)],
        ]);
    }
}
