<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(RolesTableSeeder::class);
        // $this->call(AdminSeeder::class);
        // $this->call(CategoryTableSeeder::class);
        // $this->call(SubcategoryTableSeeder::class);
        // $this->call(TypesTableSeeder::class);
        // $this->call(MediumTableSeeder::class);
        // $this->call(LevelsTableSeeder::class);
        // $this->call(LanguagesTableSeeder::class);
        // $this->call(VersionsTableSeeder::class);
        // $this->call(CoursesTableSeeder::class);
        // $this->call(CoursesVersionsSeeder::class);
        $this->call(CoursesSubcategoriesSeeder::class);
    }
}
