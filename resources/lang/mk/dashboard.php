<?php 

return [
    'admin_panel' => 'Админ Панел',
    'name' => 'Име',
    'user' => 'Корисник',
    'status' => 'Статус',
    'actions' => 'Акции',
    'dashboard' => 'Админ Панел',
    'categories' => 'Категории',
];