<?php 

return [
    'type' => 'Тип на Курс',
    'medium' => 'Медиум',
    'level' => 'Ниво',
    'version' => 'Верзија',
    'language' => 'Јазик',
    'filter_courses' => 'Филтрирај Курсеви',
    'might_be_interested' => 'Можеби си заинтересиран за',
];