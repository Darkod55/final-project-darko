<?php 

return [
    'admin_panel' => 'Admin Dashboard',
    'name' => 'Name',
    'user' => 'User',
    'status' => 'Status',
    'actions' => 'Actions',
    'categories' => 'Categories',
];