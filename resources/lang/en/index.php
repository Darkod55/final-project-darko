<?php 

return [
    'find_the_best' => 'Find the Best',
    'courses_and_tutorials' => 'Courses & Tutorials',
    'search_for_the' => 'Search for the',
    'technology' => 'technology',
    'you_want_to_learn' => 'you want to learn',
    'filter_courses' => 'Filter Courses',
    'might_be_interested' => 'You might also be interested in',
];