<?php 

return [
    'type' => 'Type of Course',
    'medium' => 'Medium',
    'level' => 'Level',
    'version' => 'Version',
    'language' => 'Language',
    'filter_courses' => 'Filter Courses',
    'might_be_interested' => 'You might also be interested in',
];