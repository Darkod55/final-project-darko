@extends('layouts.master')

@section('page-title')
    Courses - {{ $subcategory->name }}
@endsection

@section('customscripts')
<script src="{{ asset('assets/js/courses.js') }}"></script>
@endsection
@section('content')
<div class="main-container">
    <section class="breadcrumbs py-4">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-10 breadcrumbs-line">

                </div>
            </div>
        </div>
    </section>
    <section class="more-info py-3">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="row logo-sub-section">

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="py-3">
        <div class="container">
            <div class="row">
                <div class="col filter-tags">

                </div>
            </div>
        </div>
    </section>
    <section class="courses py-3">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-3 sidebar rounded border py-3 bg-color">
                            <p><strong>{{ __('courses.filter_courses') }}</strong></p>
                            <hr>
                            <div class="fortype divborder">
                                <p><strong>{{ __('courses.type') }}</strong></p>
                                <div class="typeFilters hidefilters">

                                </div>
                            </div>
                            <div class="formedium divborder">
                                <p><strong>{{ __('courses.medium') }}</strong></p>
                                <div class="mediumFilters hidefilters">
  
                                </div>
                            </div>
                            <div class="forlevel divborder">
                                <p><strong>{{ __('courses.level') }}</strong></p>
                                <div class="levelFilters hidefilters">

                                </div>
                            </div>
                            <div class="forversion">
                                <p><strong>{{ __('courses.version') }}</strong></p>
                                <div class="versionFilters hidefilters">

                                </div>
                            </div>
                            <div class="forlanguage divborder">
                                <p><strong>{{ __('courses.language') }}</strong></p>
                                <div class="languageFilters hidefilters">

                                </div>
                            </div>
                        </div> 
                        <div class="col-md-8 ml-5 border rounded py-3 class-for-mobile">
                            <div class="top-courses d-flex justify-content-between">

                            </div>
                            <hr class="my-0">
                            <div class="forappend-courses">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="py-3">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-3">

                        </div> 
                        <div class="col-md-8 ml-5 border rounded p-5 related-mobile">
                            <h4>{{ __('courses.might_be_interested') }}:</h4>
                            <div class="row forappend-related d-flex justify-content-between">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection