@extends('layouts.master')

@section('page-title')
    {{ __('dashboard.admin_panel') }}
@endsection

@section('customscripts')
<script src="{{ asset('assets/js/dashboard.js') }}"></script>
@endsection

@section('content')
<div class="main-container">
<section class="py-4">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="row user-stats pt-5 pb-5">
                        <div class="col-md-4">
                            <div class="row m-2 bg-orange col-styling-pils">
                                <div class="col-md-5 color-white d-flex justify-content-center align-items-center">
                                    <i class="fas fa-tags"></i>
                                </div>
                                <div class="col-md-5 color-white d-flex justify-content-center align-items-center flex-column">
                                    <h4 class="text-center mb-4">{{ __('global.total_number_courses') }} </h4>
                                    <h4 class="total-number">{{ count($courses) }}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row m-2 bg-green col-styling-pils">
                                <div class="col-md-5 color-white d-flex justify-content-center align-items-center">
                                    <i class="fas fa-thumbs-up"></i>
                                </div>
                                <div class="col-md-5 color-white d-flex justify-content-center align-items-center flex-column">
                                    <h4 class="text-center mb-4">{{ __('global.courses_approved') }} </h4>
                                    <h4 class="total-number-approved">{{ count($approved) }}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row m-2 bg-blue col-styling-pils">
                                <div class="col-md-5 color-white d-flex justify-content-center align-items-center">
                                    <i class="fas fa-spinner"></i>
                                </div>
                                <div class="col-md-5 color-white d-flex justify-content-center align-items-center flex-column">
                                    <h4 class="text-center mb-4">{{ __('global.courses_pending') }} </h4>
                                    <h4 class="total-number-pending">{{ count($pending) }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="tabs" class="project-tab py-3 px-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <nav>
                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="courses-approved-tab" data-toggle="tab" href="#courses-approved" role="tab" aria-controls="courses-approved" aria-selected="true">{{ __('global.courses_approved') }}</a>
                            <a class="nav-item nav-link" id="courses-pending-tab" data-toggle="tab" href="#courses-pending" role="tab" aria-controls="courses-pending" aria-selected="false">{{ __('global.courses_pending') }}</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="courses-approved" role="tabpanel" aria-labelledby="courses-approved-tab">
                            <table class="table table-bordered table-hover tablelink">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>{{ __('dashboard.name') }}</th>
                                        <th>{{ __('courses.type') }}</th>
                                        <th>{{ __('courses.medium') }}</th>
                                        <th>{{ __('courses.level') }}</th>
                                        <th>{{ __('courses.language') }}</th>
                                        <th class="max20">{{ __('dashboard.categories') }}</th>
                                        <th>{{ __('dashboard.user') }}</th>
                                        <th>{{ __('dashboard.status') }}</th>
                                        <th class="max200">{{ __('dashboard.actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody class="table-append-approved">
                                    
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="courses-pending" role="tabpanel" aria-labelledby="courses-pending-tab">
                        <table class="table table-bordered table-hover tablelink">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>{{ __('dashboard.name') }}</th>
                                        <th>{{ __('courses.type') }}</th>
                                        <th>{{ __('courses.medium') }}</th>
                                        <th>{{ __('courses.level') }}</th>
                                        <th>{{ __('courses.language') }}</th>
                                        <th class="max20">{{ __('dashboard.categories') }}</th>
                                        <th>{{ __('dashboard.user') }}</th>
                                        <th>{{ __('dashboard.status') }}</th>
                                        <th class="max200">{{ __('dashboard.actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody class="table-append-pending">
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection