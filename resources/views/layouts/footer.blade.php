<footer class="gray-bg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center">
                <p>{{ __('global.made_with') }} <i class="fas fa-heart"></i> {{ __('global.from_darko') }}</p>
            </div>
        </div>
    </div>
</footer>