<!DOCTYPE html>
<html>

<head>
    <title>@yield('page-title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='icon' href="{{ asset('assets/images/favicon.png') }}" type='image/x-icon' /> 
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    @auth
    <!-- <meta name="api-token" content="{{ \Auth::user()->api_token }}"> -->
    @endauth
    @include('layouts.style')
    @include('layouts.scripts')
</head>
<body>
    @include('layouts.header')
    @yield('customscripts')
    @yield('content')
    @include('layouts.footer') 
</body>
</html>