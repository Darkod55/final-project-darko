@extends('layouts.master')

@section('page-title')
    {{ $user->name }}
@endsection

@section('customscripts')
<script src="{{ asset('assets/js/user.js') }}"></script>
@endsection
@section('content')
<div class="main-container">
    <section class="py-4">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <h2>{{ __('global.welcome') }} {{ $user->name }}</h2>
                    <div class="row user-stats pt-5 pb-5">
                        <div class="col-md-4">
                            <div class="row m-2 bg-orange col-styling-pils">
                                <div class="col-md-5 color-white d-flex justify-content-center align-items-center">
                                    <i class="fas fa-user-tag"></i>
                                </div>
                                <div class="col-md-5 color-white d-flex justify-content-center align-items-center flex-column">
                                    <h4 class="text-center mb-4">{{ __('global.role') }} </h4>
                                    <h4>{{ $user->roles->role }}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row m-2 bg-green col-styling-pils">
                                <div class="col-md-5 color-white d-flex justify-content-center align-items-center">
                                    <i class="fas fa-plus-circle"></i>
                                </div>
                                <div class="col-md-5 color-white d-flex justify-content-center align-items-center flex-column">
                                    <h4 class="text-center mb-4">{{ __('global.courses') }} </h4>
                                    <h4>{{ count($user->courses) }}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row m-2 bg-blue col-styling-pils">
                                <div class="col-md-5 color-white d-flex justify-content-center align-items-center">
                                    <i class="fas fa-vote-yea"></i>
                                </div>
                                <div class="col-md-5 color-white d-flex justify-content-center align-items-center flex-column">
                                    <h4 class="text-center mb-4">{{ __('global.voted_for') }} </h4>
                                    <h4>{{ count($user->usersvotes) }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="tabs" class="project-tab py-4">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <nav>
                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="courses-created-tab" data-toggle="tab" href="#courses-created" role="tab" aria-controls="courses-created" aria-selected="true">{{ __('global.courses_created') }}</a>
                            <a class="nav-item nav-link" id="voted-for-tab" data-toggle="tab" href="#voted-for" role="tab" aria-controls="voted-for" aria-selected="false">{{ __('global.courses_voted_for') }}</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="courses-created" role="tabpanel" aria-labelledby="courses-created-tab">
                            <table class="table table-bordered table-hover tablelink">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>{{ __('dashboard.name') }}</th>
                                        <th>{{ __('courses.type') }}</th>
                                        <th>{{ __('courses.medium') }}</th>
                                        <th>{{ __('courses.level') }}</th>
                                        <th>{{ __('courses.language') }}</th>
                                    </tr>
                                </thead>
                                <tbody class="table-append-created">
                                    
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="voted-for" role="tabpanel" aria-labelledby="voted-for-tab">
                        <table class="table table-bordered table-hover tablelink">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>{{ __('dashboard.name') }}</th>
                                        <th>{{ __('courses.type') }}</th>
                                        <th>{{ __('courses.medium') }}</th>
                                        <th>{{ __('courses.level') }}</th>
                                        <th>{{ __('courses.language') }}</th>
                                    </tr>
                                </thead>
                                <tbody class="table-append-voted">
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection