@extends('layouts.master')

@section('page-title')
    @if(empty(\Request('slug'))) 
        {{ __('global.home') }}
    @else 
        Brainster - {{ $category->{'name_' . app()->getLocale()} }}
    @endif
@endsection

@section('content')
<div class="main-container">
<section class="forsearch py-5">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="text-center mb-4">{{ __('index.find_the_best') }} @if(!$category instanceof \Illuminate\Support\Collection) {{ $category->name }} @endif {{ __('index.courses_and_tutorials') }}</h1>
                <form>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fal fa-search"></i></span>
                        </div>
                        <input type="search" id="searchbox" class="form-control" name="search" 
                            placeholder="{{ __('index.search_for_the') }} @if(!$category instanceof \Illuminate\Support\Collection){{ $category->{'search_word_' . app()->getLocale()} }}@else {{ __('index.technology') }} @endif {{ __('index.you_want_to_learn') }}: @if(!$category instanceof \Illuminate\Support\Collection){{ $category->exm_tools }}@else Java, PHP @endif..."/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="row courses">
                    
                </div>
            </div>       
        </div>
    </div>
</section>
</div>
@endsection