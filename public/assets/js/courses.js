$(document).ready(function () {

    //Courses page - append breadcrumbs
    function appendTopBanner(value) {
        $('.breadcrumbs-line').html(`
            <a href="/${value.categories['slug']}">${value.categories['name_' + locale]}</a> > <strong>${value.name}</strong></<a>
        `)
    }

    //Courses page - section with subcategory logo
    var learn;
    var tutorials_courses;
    var online_from_the_best;
    var voted_submitted;
    if(locale == 'en') {
        learn = "Learn";
        tutorials_courses = "Tutorials and Courses";
        online_from_the_best = "online from the best";
        voted_submitted = "tutorials submitted and voted by the programming community.";
    } else if(locale == 'mk') {
        learn = "Научи";
        tutorials_courses = "Туторијали и Курсеви";
        online_from_the_best = "онлајн од најдобрите";
        voted_submitted = "туторијали креирани и гласани од програмерската заедница.";
    }
    function appendSubcategoryLogo(value) {
        $('.logo-sub-section').html(`
            <div class="col-md-1 sub-logo">
                <img src="${value.logo}" />
            </div>
            <div class="col-md-6 ml-5 d-flex flex-column justify-content-center next-sub-logo">
                <h5><strong>${value.name}</strong> ${tutorials_courses}</h5>
                <p class="mb-0">${learn} <strong>${value.name}</strong> ${online_from_the_best} 
                    <strong>${value.name}</strong> ${voted_submitted}
                </p>
            </div>
        `)
}

    //Courses page - append sidebar filters - Type
    function appendSidebarType(value) {
        value.forEach(v => {
            $('.typeFilters').append(`
                <p class="items d-flex align-items-baseline">
                    <input data-value="${v.type}" class="filter-option mr-2" type="checkbox" name="type_id" value="${v.id}"> ${v.type} (${v.number})
                </p>
            `)
        })
    }

    //Courses page - append sidebar filters - Medium
    function appendSidebarMedia(value) {
        value.forEach(v => {
            $('.mediumFilters').append(`
                <p class="items d-flex align-items-baseline">
                    <input data-value="${v.medium}" class="filter-option mr-2" type="checkbox" name="medium_id" value="${v.id}"> ${v.medium} (${v.number})
                </p>
            `)
        })
    }

    //Courses page - append sidebar filters - Level
    function appendSidebarLevel(value) {
        value.forEach(v => {
            $('.levelFilters').append(`
                <p class="items d-flex align-items-baseline">
                    <input data-value="${v.level}" class="filter-option mr-2" type="checkbox" name="level_id" value="${v.id}"> ${v.level} (${v.number})
                </p>
            `)
        })
    }

    //Courses page - append sidebar filters - Language
    function appendSidebarLanguage(value) {
        value.forEach(v => {
            $('.languageFilters').append(`
                <p class="items d-flex align-items-baseline">
                    <input data-value="${v.language}" class="filter-option mr-2" type="checkbox" name="language_id" value="${v.id}"> ${v.language} (${v.number})
                </p>
            `)
        })
    }

    //Courses page - append sidebar filters - Version
    function appendSidebarVersion(value) {
        // console.log(value);
        value.forEach(v => {
            $('.versionFilters').append(`
                <p class="items d-flex align-items-baseline">
                <input data-value="${v.version}" class="filter-option mr-2" type="checkbox" name="version_id" value="${v.id}"> ${v.version} (${v.number})
                </p>
            `)
        })
    }

    //Courses page - Right side Course title
    var tutorials;
    var top;
    var upvotes;
    var recent;
    if(locale == 'en') {
        tutorials = "Tutorials";
        top = "Top";
        upvotes = "Upvotes";
        recent = "Recent";
    } else if(locale == 'mk') {
        tutorials = "Туторијали";
        top = "Најдобри";
        upvotes = "Најмногу Гласани";
        recent = "Последни Креирани";
    }
    function appendCoursesTop(value) {
        $('.top-courses').html(`
            <p>${top} <strong>${value.name}</strong> ${tutorials}</p>
            <p class="actions"><a class="upvotes sorting active" data-sortby="views" data-sub="${value.id}">${upvotes}</a> | <a class="recent sorting" data-sortby="created_at" data-sub="${value.id}">${recent}</a></p>
        `)
    }

    //Courses page - append courses
    function appendCourses(value) {
        var submitted_by;
        if(locale == 'en') {
            submitted_by = "Submited By";
        } else if(locale == 'mk') {
            submitted_by = "Креиран од";
        }
        value.forEach(element => {
            var logged = "notvoted";
            element.coursesvotes.forEach(user => {
                if(AuthUser != false && user.pivot['user_id'] == AuthUser.id) {
                    logged = "voted";
                }
            })
            var img = "/assets/images/caret-up.svg";
            if(element.status == 1) {
                $('.forappend-courses').append(`
                    <div class="row row-course py-4 test-remove selected">
                        <div class="col-md-2 px-4 py-1 d-flex flex-column justify-content-center align-items-center voting-mobile">
                            <div data-course-id="${element.id}" data-voted="${logged}" class="${logged} forupvote d-flex w-100 h-100 flex-column justify-content-center align-items-center rounded border bg-color">
                                <img class="caret-arrow" src="${img}" />
                                <p class="mb-0 class-upvotes">${element.views}</p>
                            </div>
                        </div>
                        <div class="col-md-10 allcourses" data-courseid="${element.id}">
                            <h6><strong><a href="${element.link}">${element.name}</a></strong></h6>
                            <div>
                                <span class="gray-txt">${submitted_by}: <a href='/user/${element.users['id']}'>${element.users['name']}</a></span>
                            </div>
                            <div class="tags mt-2">
                                <span class="tags-single p-1 border rounded bg-color mr-2 my-2">${element.types['type' + "_" + locale]}</span>
                                <span class="tags-single p-1 border rounded bg-color mr-2 my-2">${element.mediums['medium' + "_" + locale]}</span>
                                <span class="tags-single p-1 border rounded bg-color mr-2 my-2">${element.levels['level' + "_" + locale]}</span>
                                <span class="tags-single p-1 border rounded bg-color mr-2 my-2">${element.languages['language' + "_" + locale]}</span>
                            </div>
                        </div>
                    </div>
                    <hr class="m-0">
                `)
            }
        }) 
    }

    //Courses page - Related Subcategories
    function appendRelated(value) {
        value.forEach(test => {
            $('.forappend-related').append(`
                <div class="item col-md-4 d-flex">
                    <div class="items-in w-100 rounded p-3 my-4">
                        <a class="d-flex align-items-center" href="/courses/${test.slug}">
                            <img src="${test.logo}" />
                            <h6 class="ml-3">${test.name}</h6>
                        </a>
                    </div>
                </div>
            `)
        })
    }

    //Courses page - All combined
    function listCourses() {
        var pathname = window.location.pathname;
        var slug = pathname.replace('/courses/','');
        //console.log(slug);
        $.post('/listCourses', {
            'slug': slug
        }, function(data) {
            console.log(data);
            appendTopBanner(data.subcategory);
            appendSubcategoryLogo(data.subcategory);
            appendSidebarType(data.finalType);
            appendSidebarMedia(data.finalMedium);
            appendSidebarLevel(data.finalLevel);
            appendSidebarLanguage(data.finalLanguage);
            appendSidebarVersion(data.finalVersions);
            appendCoursesTop(data.subcategory);
            appendCourses(data.courses);
            appendRelated(data.related);
            
        })
    }
    listCourses();

    //Sort Courses 
    $('body').on('click', ".sorting", function(e) {
        e.preventDefault();
        var subcategory_id = $(this).attr('data-sub');
        var testitem = $(this).attr('data-sortby');
        if($(".upvotes").hasClass('active')) {
            $(".upvotes").removeClass('active')
        } else {
            $(".upvotes").addClass('active')
        }
        if($(".recent").hasClass('active')) {
            $(".recent").removeClass('active')
        } else {
            $(".recent").addClass('active')
        }
        $('.forappend-courses').empty();
        $.post("/sortByFunction", {
            "subcategory_id": subcategory_id,
            "testitem": testitem
        }).then(data => {
        console.log(data);
        appendCourses(data);
        })
    })

     //Upvote Functionality
     var login_message;
     var login;
     var cancel;
    if(locale == 'en') {
        login_message = "Please log in to upvote this course!";
        login = "LOGIN";
        cancel = "CANCEL"
    } else if(locale == 'mk') {
        login_message = "Логирај се за да гласаш за овој курс!";
        login = "ЛОГИРАЈ СЕ";
        cancel = "ОТКАЖИ"
    }
     $('body').on('click', '.forupvote', function(e) {
        if(!AuthUser) {
            swal({
                type: 'error',
                title: login_message,
                showConfirmButton: true,
                showCancelButton: true,
                cancelButtonText: cancel,
                confirmButtonText: '<a href="" style="color: white;" class="swal2-confirm nav-link" data-toggle="modal" data-target="#registerModal">' + login + '</a>'
                })
            return
        }
        e.preventDefault();
        var thisElement = this;
        $(thisElement).find('.class-upvotes').empty();
        var forvote = $(this).attr('data-voted');
        var courseid = $(this).attr('data-course-id');      
        $.post('/votes/' + courseid, {
            'forvote': forvote
        }, function(data) {
            // console.log(data.views);   
            if($(thisElement).hasClass('voted')) {
                $(thisElement).removeClass('voted');
                $(thisElement).addClass('notvoted');
                $(thisElement).attr("data-voted", "notvoted");
            } else {
                $(thisElement).removeClass('notvoted');
                $(thisElement).addClass('voted');
                $(thisElement).attr("data-voted", "voted");
            } 
            $(thisElement).find('.class-upvotes').text(data['views']);
        })
    })

    $('body').on('change', '.filter-option', function(e) {
        e.preventDefault();
        var pathname = window.location.pathname;
        var slug = pathname.replace('/courses/','');
        $('.forappend-courses').empty(); 
        
        var arrayOfFilters = {};
        $(".filter-option:checked").each(function(i, e) {
            var thiswow = $(this).attr('name');
            var thisnew = $(this).attr('value');
            arrayOfFilters[thiswow] = thisnew;
        })
        arrayOfFilters['slug'] = slug;
        $(this).addClass('testhide');
        var parentDiv = $(this).parent('.hidefilters');
        console.log(parentDiv.children())
        console.log(arrayOfFilters);
        $.post('/filterOptions', arrayOfFilters, function(data) {
            console.log(data);
            appendCourses(data.courses);
        })
    })
})