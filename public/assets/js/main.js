$(document).ready(function () {

        //Appending Subcategories in Index
        function testFunction(test) {
            $('.courses').append(`
                <div class="item col-md-4 d-flex">
                    <div class="items-in w-100 rounded p-3 mb-2 mt-2">
                        <a class="d-flex align-items-center" href="/courses/${test.slug}">
                            <img src="${test.logo}" />
                            <h6 class="ml-3">${test.name}</h6>
                        </a>
                    </div>
                </div>
            `)
        }

        //Sending CSRF Token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //Function for Subcategories
        function showSubcategories() {
            var pathname = window.location.pathname;
            $.post('/showSubcategories', pathname, function(data) {
                // console.log(data);
                data.forEach(value => {
                    if("/" + value.slug == pathname) {
                        value.subcategories.forEach(test => {
                            testFunction(test);
                        })
                    } else if(pathname == "/") {
                        value.subcategories.forEach(test => {
                            testFunction(test);
                        })
                    }
                })
            })
        }
        showSubcategories();
    
        //Search Functionality
        $('#searchbox').on('keyup', function(e) {
            e.preventDefault();
            var searchvalue = $('#searchbox').val();
            // console.log(page_slug);
            // console.log(searchvalue);
            $.post('/search', {
                'search': searchvalue,
                'slug': page_slug
            }, function(data) {
                // console.log(data);
                $('.courses').empty();
                data.forEach(value => {
                    testFunction(value);
                })
            })
        })

        //Login Modal hide/show
        $('#loginModal').on('show.bs.modal', function(){
            $('#registerModal').modal('hide');
        });

        //Register Modal hide/show
        $('#registerModal').on('show.bs.modal', function(){
            $('#loginModal').modal('hide');
        });

        //For Multiselect dropdown in add new course form
        var select_technology;
        if(locale == 'en') {
            select_technology = "Select Technology";
        } else if(locale == 'mk') {
            select_technology = "Селектирај Технологија";
        } 
        $(".tools").multiselect({   
            nonSelectedText: select_technology,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            buttonWidth: '434px',
            templates: {
                filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
                filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default multiselect-clear-filter" type="button"><i class="fa fa-times-circle-o"></i></button></span>',
            }
        });

        //For Multiselect dropdown Versionsin add new course form
        var select_version;
        if(locale == 'en') {
            select_version= "Select Version";
        } else if(locale == 'mk') {
            select_version= "Селектирај Верзија";
        } 
        $(".versionform").multiselect({   
            nonSelectedText: select_version,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            buttonWidth: '434px',
            templates: {
                filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
                filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default multiselect-clear-filter" type="button"><i class="fa fa-times-circle-o"></i></button></span>',
            }
        });

        //For creating new course from header modal
        $('.createCourse').on('submit', function(e) {
            e.preventDefault();
            let objectToSubmit = {
                name: $('#name2').val(),
                link: $('#link').val(),
                subcategories: $('.tools').val(),
                versions: $('.versionform').val(),
                type: $('input[type=radio][name=type]:checked').val(),
                medium: $('input[type=radio][name=medium]:checked').val(),
                level: $('input[type=radio][name=level]:checked').val(),
                language: $('input[type=radio][name=language]:checked').val(),
            }
            var success_submit;
            if(locale == 'en') {
                success_submit= "Course successfully submitted. Your course will be reviewed by the admin!";
            } else if(locale == 'mk') {
                success_submit= "Курсот е успешно креиран. Вашиот курс ке биде разгледан од страна на админот!";
            } 
            console.log(objectToSubmit);
            $.post("/storeCourse", objectToSubmit).then(data => {
                $(".errorsmsg").hide();
                $(".successmsg").show();
                $(".successmsg").append("<li>" + success_submit + "</li>");
                $('#name').val('');
                $('#link').val('');
                $('.tools').removeAttr("checked");
                $('.versionform').removeAttr("checked");
                $('#input[type=radio]').removeAttr("checked");
                // $('#input[type=radio][name=medium]').removeAttr("checked");
                // $('#input[type=radio][name=level]').removeAttr("checked");
                // $('#input[type=radio][name=language]').removeAttr("checked");
            // let test = JSON.parse(data)
            //console.log(data)
            }).catch(function(xhr, status, error) {
                // var errors = err.responseJSON
                $(".errorsmsg").show();
                $.each(xhr.responseJSON.errors, function (key, item) {
                    $(".errorsmsg").append("<li>"+item+"</li>")
                });
                console.log(errors);
            })
        })

        //Stop Tutorial modal if user not logged in
        var logirajse;
        var logirajse_message;
        var cancel;
        if(locale == 'en') {
            logirajse = "LOGIN";
            cancel = "CANCEL";
            logirajse_message = "Please log in to submit a tutorial!";
        } else if(locale == 'mk') {
            logirajse = "ЛОГИРАЈ СЕ";
            cancel = "ОТКАЖИ";
            logirajse_message = "Логирај се за да креираш курс!";
        }
        $('.tutorial-submit').on('click', function() {
            if(!AuthUser) {
                swal({
                    type: 'error',
                    title: logirajse_message,
                    showConfirmButton: true,
                    showCancelButton: true,
                    cancelButtonText: cancel,
                    confirmButtonText: '<a href="" style="color: white;" class="swal2-confirm nav-link" data-toggle="modal" data-target="#registerModal">' + logirajse + '</a>'
                    })
                return
            }
        })
    })