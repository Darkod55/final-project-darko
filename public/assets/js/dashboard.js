$(document).ready(function () {

    //Dashboard append courses
    function appendApproved(value) {
        var approved;
        var dataApprove;
        var textApprove;
        var buttonClass;
        var divAppend;
        if(value.status == 1) {
            if(locale == 'en') {
                approved = "Approved";
            } else if(locale == 'mk') {
                approved = "Одобрен";
            }
            dataApprove = "notApproved";
            textApprove = "<i class='fas fa-thumbs-down'></i>";
            buttonClass = "btn-info";
            divAppend = ".table-append-approved"
        } else if(value.status == 0) {
            if(locale == 'en') {
                approved = "Not Approved";
            } else if(locale == 'mk') {
                approved = "Неодобрен";
            }
            dataApprove = "approved";
            textApprove = "<i class='fas fa-thumbs-up'></i>";
            buttonClass = "btn-warning";
            divAppend = ".table-append-pending"
        }
        var test = [];
        value.subcategories.forEach(v => {
            test.push(v.name);
        })
        console.log(test);
        $(divAppend).append(`
            <tr>
                <td>${value.id}</td>
                <td class="name-dashboard"><a href="${value.link}">${value.name}</a></td>
                <td>${value.types['type' + '_' + locale]}</td>
                <td>${value.mediums['medium' + '_' + locale]}</td>
                <td>${value.levels['level' + '_' + locale]}</td>
                <td>${value.languages['language' + '_' + locale]}</td>
                <td>${test}</td>
                <td>${value.users['name']}</td>
                <td>${approved}</td>
                <td class="text-center foractions">
                    <a class="approveAction btn ${buttonClass}" data-approve="${dataApprove}" data-table="${divAppend}" data-id="${value.id}">${textApprove}</a>
                    <a class="adminDelete btn btn-danger" data-id="${value.id}"><i class="fas fa-trash"></i></a>
                </td>
            </tr>
        `)
    }

    //Dashboard show courses
    function showInDashboard() {
        $.post('/showInDashboard', function(data) {
            //console.log(data);
            data.forEach(value => {
                appendApproved(value);
            })
        })
    }
    showInDashboard();

    function appendNumbersDelete(value) {
        $('.total-number').append(value.length)
    }

    function appendNumbersCourses(value) {
        var forApproved = [];
        var forPending = [];
        var textDynamic;
        value.forEach(v => {
            if(v.status == 0) {
                forPending.push(v);
                textDynamic = ".total-number-pending";
            } else {
                forApproved.push(v);
                textDynamic = ".total-number-approved";
            }
        })
        $('.total-number').append(value.length)
        $(".total-number-pending").append(forPending.length)
        $(".total-number-approved").append(forApproved.length)
    }

    //Dashboard Delete course
    var deleted;
    if(locale == 'en') {
        deleted = "The course has been deleted!";
    } else if(locale == 'mk') {
        deleted = "Курсот е избришан!";
    }
    $('body').on('click', '.adminDelete', function(e) {
        e.preventDefault();
        var course_id = $(this).attr('data-id');
        $('.table-append-approved').empty();
        $('.table-append-pending').empty();
        $('.total-number').empty();
        $('.total-number-approved').empty();
        $('.total-number-pending').empty();
        console.log(course_id);
        $.post('/adminDelete', {
            'id': course_id
        }, function(data) {
            swal({
                type: 'error',
                title: deleted,
                showConfirmButton: true,
                })
            console.log(data);
            data.forEach(value => {
                appendApproved(value);
            })
            appendNumbersCourses(data);
        })
    })


    //Dashboard approve or disapprove course
    $('body').on('click', '.approveAction', function(e) {
        e.preventDefault();
        var course_id = $(this).attr('data-id');
        var approve_action = $(this).attr('data-approve');
        var approving;
        var status;
        if(locale == 'en') {
            if(approve_action == "approved") {
                approving = "The course has been approved!";
                status = 'success'
            } else {
                approving = "The course has been disapproved!"; 
                status = 'error'
            }
        } else if(locale == 'mk') {
            if(approve_action == "approved") {
                approving = "Курсот е одобрен!";
                status = 'success'
            } else {
                approving = "Курсот не е одобрен!"; 
                status = 'error'
            }
        }
        $('.table-append-approved').empty();
        $('.table-append-pending').empty();
        $('.total-number').empty();
        $('.total-number-approved').empty();
        $('.total-number-pending').empty();
        // $(divEmptu).empty();
        // console.log(approve_action);
        $.post('/adminApprove', {
            'id': course_id,
            'approve_action': approve_action
        }, function(data) {
            swal({
                type: status,
                title: approving,
                showConfirmButton: true,
                })
            console.log(data);
            data.forEach(value => {
                appendApproved(value);
            })
            appendNumbersCourses(data)
        })
    })
})