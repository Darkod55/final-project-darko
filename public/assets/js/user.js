$(document).ready(function () {
    

    //User append courses
    function appendUserCourses(element) {
        element.forEach(value => {
            $('.table-append-created').append(`
                <tr>
                    <td>${value.id}</td>
                    <td class="name-dashboard"><a href="${value.link}">${value.name}</a></td>
                    <td>${value.types['type' + '_' + locale]}</td>
                    <td>${value.mediums['medium' + '_' + locale]}</td>
                    <td>${value.levels['level' + '_' + locale]}</td>
                    <td>${value.languages['language' + '_' + locale]}</td>
                </tr>
            `)
        })
    }

    //User append voted
    function appendUserVoted(element) {
        element.forEach(value => {
            $('.table-append-voted').append(`
                <tr>
                    <td>${value.id}</td>
                    <td class="name-dashboard"><a href="${value.link}">${value.name}</a></td>
                    <td>${value.types['type' + '_' + locale]}</td>
                    <td>${value.mediums['medium' + '_' + locale]}</td>
                    <td>${value.levels['level' + '_' + locale]}</td>
                    <td>${value.languages['language' + '_' + locale]}</td>
                </tr>
            `)
        })
    }

    //User show courses
    function showForUser() {
        var pathname = window.location.pathname;
        var user_id = pathname.replace('/user/','');
        $.post('/showForUser', {
            'user_id': user_id
         }, function(data) {
            console.log(data);
            // data.forEach(value => {
                appendUserCourses(data.courses);
                appendUserVoted(data.usersvotes);
            // })
        })
    }
    showForUser();
})