<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Courses_Versions extends Model
{
    public function courses()
    {
        return $this->belongsTo('Course');
    }

    public function versions()
    {
        return $this->belongsTo('Version');
    }
}
