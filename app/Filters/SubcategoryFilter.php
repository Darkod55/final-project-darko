<?php

namespace App\Filters;
class SubcategoryFilter
{
    public function filter($builder, $value)
    {
        return $builder->where('subcategory_id', $value);
    }
}
?>