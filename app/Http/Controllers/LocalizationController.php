<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use URL;

class LocalizationController extends Controller
{
    public function setLocale($locale='en'){
        if (!in_array($locale, ['en', 'mk', 'de'])){
            $locale = 'en';
        }
        Session::put('locale', $locale);
        \App::setLocale($locale);
        // dd($locale);
        return redirect(url(URL::previous()));
    }
}
