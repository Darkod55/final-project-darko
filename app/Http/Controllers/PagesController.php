<?php

namespace App\Http\Controllers;
use App\Category;
use App\Subcategory;
use App\Course;
use App\User;
use App\CoursesSubcategories;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    {   
        if(\Request('slug') == 'dashboard') {
            // dd(\Request('slug'));
        return redirect()->route('dashboard');
        } 
        
        if(empty(\Request('slug'))) {
            $category = Category::with('subcategories')->get();
            $slug = "/";
            return view('index', compact('category', 'slug'));
        } else if(strpos(\Request('slug'), 'courses') !== false) {
            return redirect()->route('courses');
        }  else if(\Request('slug') != 'dashboard') {
            $category = Category::with('subcategories')->where('slug', \Request('slug'))->first();
            $slug = $category->slug;
            return view('index', compact('category', 'slug'));
        }
        
    }

    public function showSubcategories() {
        if(empty(\Request('slug'))) {
            $category = Category::with('subcategories')->get();
        } else {
            $category = Category::with('subcategories')->where('slug', \Request('slug'))->first();
        }
        return response()->json($category);
    }
    
    public function search(Request $request) {
        if($request->slug == "/") {
            $subcategories = Subcategory::where('name', 'LIKE', '%'.$request->get('search').'%')->get();;
        } else {
            $category = Category::where('slug', trim($request->slug, "/"))->first()->id;
            $subcategories = Subcategory::with('categories')->where('category_id', $category)
            ->where('name', 'LIKE', '%'.$request->get('search').'%')
            ->get();
        }
        
        return response()->json($subcategories);
    }
}
