<?php

namespace App\Http\Controllers;
use App\User;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request) {
        $user = User::with('courses', 'usersvotes')->where('id', $request->id)->first();
        // dd($user->courses);
        // dd($user);
        // dd(\Request('id'));
        return view('user', ['user' => $user]);
    }

    public function showForUser(Request $request) {
        $user = User::with('courses', 'usersvotes')->where('id', $request->user_id)->first();
        // $courses = Course::whereorderBy('status', "ASC")->get();
        return response()->json($user);
    }
}
