<?php

namespace App\Http\Controllers;
use App\Course;
use App\CoursesSubcategories;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function showDashboard() {
        $courses = Course::all();
        $approved = [];
        $pending = [];
        foreach($courses as $c) {
            if($c->status == 0) {
                $pending[] = $c;
            } else {
                $approved[] = $c;
            }
        }
        return view('/dashboard', ['courses' => $courses, 'approved' => $approved, 'pending' => $pending]);
    }

    public function showInDashboard() {
        $courses = Course::orderBy('status', "ASC")->get();
        return response()->json($courses);
    }

    public function adminDelete(Request $request) {
        $test = Course::find($request->id);
        $test->subcategories()->detach();
        $test->versions()->detach();
        $test->coursesvotes()->detach();
        
        Course::destroy($request->id);
        
        $courses = Course::all();
        return response()->json($courses);
    }

    public function adminApprove(Request $request) {
        if($request->approve_action == "notApproved") {
            Course::where('id', $request->id)->update([
                "status" => 0
            ]);
        } else if($request->approve_action == "approved") {
            Course::where('id', $request->id)->update([
                "status" => 1
            ]);
        }

        $courses = Course::orderBy('status', "ASC")->get();
        return response()->json($courses);
    }
}
